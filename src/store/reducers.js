import warehouses from "./warehouses/reducer";
import products from "./products/reducer";

export { warehouses, products };
