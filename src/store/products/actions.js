import uuid from "uuid/v4";
import * as types from "./actionTypes";
import * as productsApi from "../../api/products";
import * as warehousesApi from "../../api/warehouses";

export function fetchProducts() {
  return async (dispatch, getState) => {
    try {
      if (getState().products.list.pagination.loading || (getState().products.list.pagination.totalCount !== 0 && getState().products.list.pagination.totalCount <= getState().products.list.items.length)) return;

      dispatch({ type: types.SET_LIST_ERROR, error: null });
      dispatch({ type: types.SET_LIST_LOADING, loading: true });

      const products = await productsApi.fetchAllProductsList({
        page: getState().products.list.pagination.page,
        limit: getState().products.list.pagination.limit
      });

      dispatch({ type: types.SET_LIST_ITEMS, items: [...getState().products.list.items, ...products.items] });
      dispatch({ type: types.SET_LIST_PAGE, page: getState().products.list.pagination.page + 1 });
      dispatch({ type: types.SET_LIST_TOTAL_COUNT, totalCount: products.totalCount });
      dispatch({ type: types.SET_LIST_LOADING, loading: false });
    } catch (error) {
      dispatch({ type: types.SET_LIST_ERROR, error });
      dispatch({ type: types.SET_LIST_LOADING, loading: false });
    }
  };
}

export function fetchWarehousesBySelectedProduct() {
  return async (dispatch, getState) => {
    try {
      const state = getState();

      dispatch({ type: types.SET_SELECTED_WAREHOUSES_ERROR, error: null });
      dispatch({ type: types.SET_SELECTED_WAREHOUSES_LOADING, loading: true });

      let warehouses = await warehousesApi.fetchWarehousesListByProduct({
        productId: state.products.selected.item.id,
        page: state.products.selected.warehouses.pagination.page,
        limit: state.products.selected.warehouses.pagination.limit
      });

      dispatch({ type: types.SET_SELECTED_WAREHOUSES_ITEMS, items: [...state.products.selected.warehouses.items, ...warehouses.items] });
      dispatch({ type: types.SET_SELECTED_WAREHOUSES_LOADING, loading: false });
    } catch (error) {
      dispatch({ type: types.SET_SELECTED_WAREHOUSES_ERROR, error });
      dispatch({ type: types.SET_SELECTED_WAREHOUSES_LOADING, loading: false });
    }
  };
}

export function clearProducts() {
  return dispatch => {
    dispatch({ type: types.SET_LIST_ITEMS, items: [] });
    dispatch({ type: types.SET_SELECTED_WAREHOUSES_ITEMS, items: [] });
  };
}

export function clearSelectedProduct() {
  return dispatch => {
    dispatch({ type: types.SET_SELECTED_ITEM, product: null });
    dispatch({ type: types.SET_SELECTED_ERROR, error: null });
    dispatch({ type: types.SET_SELECTED_LOADING, loading: false });
    dispatch({ type: types.SET_SELECTED_WAREHOUSES_ITEMS, items: [] });
    dispatch({ type: types.SET_SELECTED_WAREHOUSES_ERROR, error: null });
    dispatch({ type: types.SET_SELECTED_WAREHOUSES_LOADING, loading: false });
    dispatch({ type: types.SET_SELECTED_WAREHOUSES_PAGE, page: 1 });
    dispatch({ type: types.SET_SELECTED_WAREHOUSES_TOTAL_COUNT, totalCount: 0 });
    dispatch({ type: types.SET_SELECTED_WAREHOUSES_FOR_DELETE, warehousesForDelete: [] });
  };
}

export function createProduct({ title, quantity, warehousesDistributions }) {
  return async (dispatch, getState) => {
    try {
      dispatch({ type: types.SET_SELECTED_ERROR, error: null });
      dispatch({ type: types.SET_SELECTED_LOADING, loading: true });

      for (let warehousesDistribution of warehousesDistributions) {
        if (!warehousesDistribution.warehouse) throw new Error("Not all warehouses are selected");
      }

      const newProduct = await productsApi.createProduct({ title, quantity, warehousesDistributions: getState().products.selected.warehouses.items });

      if (getState().products.list.pagination.totalCount === getState().products.list.items.length) {
        dispatch({ type: types.SET_LIST_ITEMS, items: [...getState().products.list.items, newProduct] });
      }

      dispatch({ type: types.SET_LIST_TOTAL_COUNT, totalCount: getState().products.list.pagination.totalCount + 1 });
      dispatch({ type: types.SET_SELECTED_LOADING, loading: false });
    } catch (error) {
      dispatch({ type: types.SET_SELECTED_ERROR, error });
      dispatch({ type: types.SET_SELECTED_LOADING, loading: false });
    }
  };
}

export function editProduct({ title, quantity, warehousesDistributions, warehousesDistributionsToDelete }) {
  return async (dispatch, getState) => {
    try {
      dispatch({ type: types.SET_SELECTED_ERROR, error: null });
      dispatch({ type: types.SET_SELECTED_LOADING, loading: true });

      if (getState().products.selected.warehouses.items.findIndex(warehouseItem => !warehouseItem.warehouse) !== -1) throw new Error("Not all warehouse are selected, check the fields");

      const id = getState().products.selected.item.id;
      const items = getState().products.list.items;
      const editProductIndexInArray = items.findIndex(item => item.id === id);
      const product = await productsApi.editProduct({
        id,
        title,
        quantity,
        warehousesDistributions: getState().products.selected.warehouses.items,
        warehousesForDelete: getState().products.selected.warehousesForDelete
      });

      items[editProductIndexInArray] = product;

      dispatch({ type: types.SET_LIST_ITEMS, items: [...items] });
      dispatch({ type: types.SET_SELECTED_LOADING, loading: false });
    } catch (error) {
      dispatch({ type: types.SET_SELECTED_ERROR, error });
      dispatch({ type: types.SET_SELECTED_LOADING, loading: false });
    }
  };
}

export function removeProduct() {
  return async (dispatch, getState) => {
    try {
      dispatch({ type: types.SET_SELECTED_ERROR, error: null });
      dispatch({ type: types.SET_SELECTED_LOADING, loading: true });

      await productsApi.removeProduct(getState().products.selected.item);

      dispatch({ type: types.SET_LIST_TOTAL_COUNT, totalCount: getState().products.list.pagination.totalCount - 1 });
      dispatch({ type: types.SET_LIST_ITEMS, items: [...getState().products.list.items.filter(item => item.id !== getState().products.selected.item.id)] });
      dispatch({ type: types.SET_SELECTED_LOADING, loading: false });
    } catch (error) {
      dispatch({ type: types.SET_SELECTED_ERROR, error });
      dispatch({ type: types.SET_SELECTED_LOADING, loading: false });
    }
  };
}

export function addWarehouseForProduct() {
  return async (dispatch, getState) => {
    const productWarehouse = getState().products.selected.warehouses.items;

    productWarehouse.push({
      id: uuid(),
      warehouse: null,
      quantity: 1,
      isNew: true
    });

    dispatch({
      type: types.SET_SELECTED_WAREHOUSES_ITEMS,
      items: [...productWarehouse]
    });
  };
}

export function editWarehouseForProduct({ warehouseDistributions, warehouse, quantity }) {
  return async (dispatch, getState) => {
    if (warehouse) warehouseDistributions.warehouse = warehouse;
    if (quantity >= 0) warehouseDistributions.quantity = quantity;

    warehouseDistributions["edited"] = true;

    dispatch({
      type: types.SET_SELECTED_WAREHOUSES_ITEMS,
      items: [...getState().products.selected.warehouses.items]
    });
  };
}

export function removeWarehouseFromProduct(warehouseDistributions) {
  return async (dispatch, getState) => {
    const productsWarehouse = getState().products.selected.warehouses.items;
    const indexOfWarehouseDistributions = productsWarehouse.indexOf(warehouseDistributions);

    if (indexOfWarehouseDistributions === -1) return console.warn("No found warehouseDistributions for remove");

    productsWarehouse.splice(indexOfWarehouseDistributions, 1);

    dispatch(addProductForDelete(warehouseDistributions));
    dispatch({
      type: types.SET_SELECTED_WAREHOUSES_ITEMS,
      items: [...productsWarehouse]
    });
  };
}

export function addProductForDelete(warehouseDistributions) {
  return async (dispatch, getState) => {
    if (!warehouseDistributions.isNew) {
      const warehousesForDelete = getState().products.selected.warehousesForDelete;

      warehousesForDelete.push(warehouseDistributions);

      dispatch({
        type: types.SET_SELECTED_WAREHOUSES_FOR_DELETE,
        warehousesForDelete: [...warehousesForDelete]
      });
    }
  };
}
