import React, { Component } from "react";
import { Button, Pagination } from "react-bootstrap";
import { connect } from "react-redux";

import ProductsDistributionElement from "./ProductsDistributionElement";
import * as warehousesActions from "../store/warehouses/actions";
import Loader from "./Loader";

class ProductsDistributionList extends Component {
  onClickAddProductDistribution() {
    this.props.dispatch(warehousesActions.addProductForWarehouse());
  }

  render() {
    return (
      <>
        <div style={{ position: "relative" }}>
          {this.props.selectedWarehouseProducts.map(item => (
            <ProductsDistributionElement key={item.id} productDistributions={item} />
          ))}
          {this.props.selectedWarehouseLoading && <Loader />}
        </div>
        {!this.props.selectedWarehouseLoading && (
          <Button block className="mt-3" onClick={() => this.onClickAddProductDistribution()}>
            Add product
          </Button>
        )}
        <div className="d-flex justify-content-center mt-3">
          <Pagination className="mb-0">
            {[1, 1, 1, 1].map((item, index) => (
              <Pagination.Item key={index}>{index}</Pagination.Item>
            ))}
          </Pagination>
        </div>
      </>
    );
  }
}

function mapStateToProps(state) {
  return {
    selectedWarehouseProducts: state.warehouses.selected.products.items,
    selectedWarehouseLoading: state.warehouses.selected.products.loading,
    // selectedWarehouseError: state.warehouses.selected.products.error
  };
}

export default connect(mapStateToProps)(ProductsDistributionList);
