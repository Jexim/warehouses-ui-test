import React, { Component } from "react";
import { Button } from "react-bootstrap";
import { connect } from "react-redux";

import WarehousesDistributionElement from "./WarehousesDistributionElement";
import * as productsActions from "../store/products/actions";
import Loader from "./Loader";

class WarehousesDistributionList extends Component {
  onClickAddWarehousesDistribution() {
    this.props.dispatch(productsActions.addWarehouseForProduct());
  }

  render() {
    // const freeQuantity = this.props.quantity - this.state.sumQuantity;

    return (
      <>
        <div style={{ position: "relative" }}>
          {this.props.selectedProductWarehouses.map(item => (
            <WarehousesDistributionElement
              key={item.id}
              warehouseDistributions={item}
            />
          ))}
          {this.props.selectedProductLoading && <Loader />}
        </div>
        {/* {freeQuantity > 0 && <div className="mb-3">Free count: {freeQuantity}</div>} */}
        {!this.props.selectedProductLoading && (
          <Button block onClick={() => this.onClickAddWarehousesDistribution()}>
            Add to warehouse
          </Button>
        )}
      </>
    );
  }
}

function mapStateToProps(state) {
  return {
    selectedProductWarehouses: state.products.selected.warehouses.items,
    selectedProductLoading: state.products.selected.warehouses.loading,
    // selectedProductError: state.products.selected.warehouses.error
  };
}

export default connect(mapStateToProps)(WarehousesDistributionList);
